import React, { useState, useEffect } from 'react'
import config from 'config'

const checkAuthenticated = props => WrappedComponent => {
  // const [isAuthenticated, setIsAuthenticated] = useState(false)
  class CheckAuthenticated extends React.Component {
    state = {
      isAuthenticated: false
    }

    // static getDe
    componentWillMount() {
      console.log('==componentWillMount==')
      window.location.href = `https://fusionauth.ilotusland.asia/oauth2/authorize?client_id=${config.clientID}&redirect_uri=${config.redirectURI}&response_type=code`
      console.log(`http://ilotusland.localhost:${config.serverPort}/login`)
    }
    // async componentDidMount() {
    //   console.log('====start chefck authen')
    //   console.log(this.state.isAuthenticated)

    // }
    render() {
      return <WrappedComponent />
    }
  }
  return CheckAuthenticated
}

// const checkAuthenticated = props => WrappedComponent => {
//   const [isAuthenticated, setIsAuthenticated] = useState(false)
//   useEffect(() => {

//   })
//   if (!isAuthenticated) {
//     return <div>unauthorized</div>
//   }
//   return <WrappedComponent />
// }


export default checkAuthenticated