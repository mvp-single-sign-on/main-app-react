import React, { useState, useEffect } from 'react'
import { ApplicationsArea } from './applications'
import { Header } from 'components/header'
import config from 'config'




const userInfo = {
  fullName: 'Huy Cuong',
  avatar: '/images/user-avatar/sample.svg'
}
const logo = '/images/app-icon/ilotusland.svg'


const Home = () => {
  const [userInfo, setUserInfo] = useState('')

  useEffect(() => {
    // window.location.href = `http://ilotusland.localhost:${config.serverPort}/login`
    const fetchData = async () => {
      const userResponse = await fetch(`http://ilotusland.localhost:${config.serverPort}/user`, {
        credentials: 'include' // fetch won't send cookies unless you set credentials
      })
      const userInfoResponse = await userResponse.json()
      if (userInfoResponse.data) {
        // console.log(userInfo, '==userInfo==rese')
        setUserInfo(userInfoResponse.data)
      }
      else {
        console.log('===redireing to logi ....')
        window.location.href = `http://ilotusland.localhost:${config.serverPort}/login`

      }

      // console.log(userInfo, '==userInfo===')
    }
    fetchData()

  }, [])
  return (
    <div>
      <Header logo={logo} avatar={userInfo.picture} fullName={userInfo.email} />
      <ApplicationsArea />
    </div>
  )
}

export { Home }