import styled from 'styled-components'
import React, { useEffect, useState } from 'react'
import { AppItem } from 'components/application-item'


const BACKEND_URL = 'http://103.89.85.226:3001'

const AppWrapper = styled.div`
margin-left:151px;
margin-right:151px;
margin-top:136px;
padding:32px;
background:red;
width:100%;
height:100%;
background:#FAFBFB;
display: flex;
flex-wrap: wrap;
`

// const data = [
//   {
//     logo: '/images/app-icon/collecting.svg',
//     name: 'Collecting',
//     description: 'connect any data',
//     url: 'https://google.com'
//   },
//   {
//     logo: '/images/app-icon/camera.svg',
//     name: 'Camera',
//     description: 'streaming camera',
//     url: 'http://camera.ilotusland.localhost:9001/login'
//   },
//   {
//     logo: '/images/app-icon/control.svg',
//     name: 'Control',
//     description: 'control data station',
//     url: 'https://google.com'
//   },
//   {
//     logo: '/images/app-icon/report.svg',
//     name: 'Report',
//     description: 'build, and export report',
//     url: 'http://report.ilotusland.localhost:9002/login'
//   },
//   {
//     logo: '/images/app-icon/billing.svg',
//     name: 'Billing',
//     description: 'invoice, payment',
//     url: 'https://google.com'
//   },
//   {
//     logo: '/images/app-icon/incident.svg',
//     name: 'Incidents',
//     description: 'critical of the system',
//     url: 'https://google.com'
//   }
// ]

const ApplicationsArea = (Component) => {
  const [data, setData] = useState([])
  const fetchData = async () => {
    const appRes = await fetch(`${BACKEND_URL}/app`)
    const apps = await appRes.json()
    const adonRes = await fetch(`${BACKEND_URL}/addon`)
    const addons = await adonRes.json()
    const result = [...apps, ...addons]
    return result.filter(app => app.isEnable === true)
    // return [...apps, ...addons]
  }
  useEffect(() => {

    fetchData().then(res => {
      setData(res)
    })
  }, [])
  return <AppWrapper>
    {
      data.map(app => <AppItem onClick={() => { window.location.href = app.webUrl }} logo={app.logo} name={app.name} description={app.description} />)
    }
    {/* <AppItem /> */}
  </AppWrapper>

}

export { ApplicationsArea }