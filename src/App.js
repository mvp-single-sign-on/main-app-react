import React, { useState, useEffect } from "react";
import "./App.scss";
import { Home } from "containers/home";
import { LoginPage } from 'containers/login'
import { BrowserRouter as Router, Route } from "react-router-dom";
import checkAuthenticated from 'hoc/auth'
function App() {
  // const [isAuthenticated,getIsAuthenticated]=useState(false)
  // useEffect(()=>{

  // })
  return (
    <Router>
      <div className="App">
        <Route path="/" exact component={Home} />
        <Route path="/login" exact component={LoginPage} />
      </div>
    </Router>
  );
}

// export default checkAuthenticated()(App)
export default App
