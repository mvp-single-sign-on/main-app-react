import styled from 'styled-components'
import React from 'react'
import { Profile } from './user-profile'
import { Button } from 'antd';
import config from 'config'
import { useHistory } from "react-router-dom";



const HeaderWrapper = styled.div`
position: absolute;
width: 1440px;
height: 64px;
left: 0px;
top: 0px;

background: #FFFFFF;
box-shadow: 0px 4px 10px rgba(219, 219, 219, 0.25);

display:flex;
justify-content: space-between;

img{
  margin-left:16px;
}

.user-profile{
  /* margin-right:14px; */
}
`

const Logout = ({ url }) => {
  let history = useHistory();
  // return <Button onClick={() => window.location.href = `http://ilotusland.localhost:${config.serverPort}/login`} type="primary">Login</Button>
  // return <Button onClick={() => history.push('/login')} type="primary">Logout</Button>
  console.log(`main app logout url========> http://ilotusland.localhost:${config.serverPort}/logout`)
  return <Button onClick={() => window.location.href = `http://ilotusland.localhost:${config.serverPort}/logout`} type="primary">Logout</Button>
}
const Header = ({ logo, avatar, fullName, url }) => {
  return <HeaderWrapper>
    <img src={logo} alt='app logo' />
    {
      avatar && fullName && (
        <>
          <Profile className='user-profile' avatar={avatar} fullName={fullName} />
          <Logout className='user-profile' />
        </>
      )

    }
  </HeaderWrapper>
}

export { Header }

